#include "Maze.h"

#include <iostream>


SquareMaze::SquareMaze(Size sz):mtxSpots(sz)
{
    Point p;
    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
    {
        mtxSpots[p].vLinks = std::vector<Link>(4);
    }

    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
    {
        if(p.x != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x - 1, p.y);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            mtxSpots[pCurr].vLinks[0].pPass = pPass;
            mtxSpots[pCurr].vLinks[0].pTo   = &(mtxSpots[pPrev]);

            mtxSpots[pPrev].vLinks[1].pPass = pPass;
            mtxSpots[pPrev].vLinks[1].pTo   = &(mtxSpots[pCurr]);

        }

        if(p.y != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x, p.y - 1);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            mtxSpots[pCurr].vLinks[2].pPass = pPass;
            mtxSpots[pCurr].vLinks[2].pTo   = &(mtxSpots[pPrev]);

            mtxSpots[pPrev].vLinks[3].pPass = pPass;
            mtxSpots[pPrev].vLinks[3].pTo   = &(mtxSpots[pCurr]);

        }
    }
}

void SquareMaze::GetMaze(Maze& m)
{
    m = Maze();

    Point p;
    for(p.x = 0; p.x < mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mtxSpots.GetSize().y; ++p.y)
        m.vSpots.push_back( &(mtxSpots[p]) );
}


bool IsWalled(Pass* pPass)
{
    return (!pPass) || pPass->bWalled;
}

void Draw(SquareMaze& sq, SP< GraphicalInterface<IndexImg> > pGr, Point pOffset, Size szElement)
{
    Size sz = szElement;
    
    Point pi;
    for(pi.x = 0; pi.x < sq.mtxSpots.GetSize().x; ++pi.x)
    for(pi.y = 0; pi.y < sq.mtxSpots.GetSize().y; ++pi.y)
    {
        Point p = pOffset + Point(pi.x * sz.x, pi.y * sz.y);

        if(IsWalled(sq.mtxSpots[pi].vLinks[0].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[1].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x, p.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[2].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + sz.x + 1, p.y + 1), Color(), false);
        if(IsWalled(sq.mtxSpots[pi].vLinks[3].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y + sz.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
    }
}

CubeMaze::CubeMaze(Size sz_, int nHeight): cbSpots(nHeight, Matrix<Spot>(sz_)), sz(sz_)
{
    Point p; unsigned nH;
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
        cbSpots[nH][p].vLinks = std::vector<Link>(6);
    
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
    {
        if(p.x != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x - 1, p.y);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][pCurr].vLinks[0].pPass = pPass;
            cbSpots[nH][pCurr].vLinks[0].pTo   = &(cbSpots[nH][pPrev]);

            cbSpots[nH][pPrev].vLinks[1].pPass = pPass;
            cbSpots[nH][pPrev].vLinks[1].pTo   = &(cbSpots[nH][pCurr]);

        }

        if(p.y != 0)
        {
            Point pCurr = p;
            Point pPrev = Point(p.x, p.y - 1);
            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][pCurr].vLinks[2].pPass = pPass;
            cbSpots[nH][pCurr].vLinks[2].pTo   = &(cbSpots[nH][pPrev]);

            cbSpots[nH][pPrev].vLinks[3].pPass = pPass;
            cbSpots[nH][pPrev].vLinks[3].pTo   = &(cbSpots[nH][pCurr]);

        }

        if(nH != 0)
        {            
            Pass* pPass = new Pass(true);
            vPasses.push_back(pPass);
            
            cbSpots[nH][p].vLinks[4].pPass = pPass;
            cbSpots[nH][p].vLinks[4].pTo   = &(cbSpots[nH - 1][p]);

            cbSpots[nH - 1][p].vLinks[5].pPass = pPass;
            cbSpots[nH - 1][p].vLinks[5].pTo   = &(cbSpots[nH][p]);

        }
    }
}

void CubeMaze::GetMaze(Maze& m)
{
    m = Maze();

    Point p; unsigned nH;
    for(nH = 0; nH < cbSpots.size(); ++nH)
    for(p.x = 0; p.x < sz.x; ++p.x)
    for(p.y = 0; p.y < sz.y; ++p.y)
        m.vSpots.push_back( &(cbSpots[nH][p]) );
}

void Draw(CubeMaze& sq, SP< GraphicalInterface<IndexImg> > pGr, Point pOffset, Size szElement, int nLvl)
{
    Size sz = szElement;
    
    Point pi;
    for(pi.x = 0; pi.x < sq.sz.x; ++pi.x)
    for(pi.y = 0; pi.y < sq.sz.y; ++pi.y)
    {
        Point p = pOffset + Point(pi.x * sz.x, pi.y * sz.y);

        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[0].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[1].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x, p.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[2].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y, p.x + sz.x + 1, p.y + 1), Color(), false);
        if(IsWalled(sq.cbSpots[nLvl][pi].vLinks[3].pPass))
            pGr->DrawRectangle(Rectangle(p.x, p.y + sz.y, p.x + sz.x + 1, p.y + sz.y + 1), Color(), false);
        if(!IsWalled(sq.cbSpots[nLvl][pi].vLinks[5].pPass))
            pGr->DrawRectangle(Rectangle(p.x + sz.x/4, p.y + sz.y/4, p.x + sz.x*3/4, p.y + sz.y*3/4), Color(), false);
        if(!IsWalled(sq.cbSpots[nLvl][pi].vLinks[4].pPass))
            pGr->DrawRectangle(Rectangle(Point(p.x + sz.x/2, p.y + sz.y/2), Size(2,2)), Color(255,0,0), false);
    }
}

int RandomWalk(Spot* pSpot)
{
    int sz = pSpot->vLinks.size();
    int n;

    while(true)
    {
        /*
        if(sz == 4)
        {
            int k = rand()%22;
            if(k < 10)
                n = 0;
            else if(k < 20)
                n = 1;
            else if(k == 20)
                n = 2;
            else
                n = 3;
        }
        else if(sz == 6)
        {
            int k = rand()%402;
            if(k < 100)
                n = 0;
            else if(k < 200)
                n = 1;
            else if(k < 300)
                n = 2;
            else if(k < 400)
                n = 3;
            else if(k == 400)
                n = 4;
            else
                n = 5;
        
        }
        else
        */
            n = rand()%sz;

        if(pSpot->vLinks[n].pPass)
            break;
    }

    return n;
}

bool IsIsolated(Spot* pSpot)
{
    unsigned j;
    for(j = 0; j < pSpot->vLinks.size(); ++j)
        if(!IsWalled(pSpot->vLinks[j].pPass))
            break;
    return j == pSpot->vLinks.size();
}


void AldousBroderMazeStep(Spot*& pSpot)
{
    Link* pLink = &(pSpot->vLinks[RandomWalk(pSpot)]);
    Spot* pNewSpot = pLink->pTo;

    if(IsIsolated(pNewSpot))
        pLink->pPass->bWalled = false;

    pSpot = pNewSpot;
}


void AldousBroderMaze(Maze& mz)
{
    Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];
    
    while(true)
    {
        unsigned i;
        for(i = 0; i < mz.vSpots.size(); ++i)
            if(IsIsolated(mz.vSpots[i]))
                break;

        if(i == mz.vSpots.size())
            return;

        AldousBroderMazeStep(pSpot);
    }
}

bool WilsonMazeStep(Maze& mz, Spot* pInitSpot)
{
    std::vector<Spot*> vIsolated;
    
    unsigned i;
    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        if(mz.vSpots[i] == pInitSpot)
            continue;
        
        if(IsIsolated(mz.vSpots[i]))
            vIsolated.push_back(mz.vSpots[i]);
    }

    if(vIsolated.empty())
        return false;

    Spot* pCurrSpot = vIsolated[rand()%vIsolated.size()];
    Spot* pStarSpot = pCurrSpot;
    std::vector<int> vPath;

    while(true)
    {
        int n = RandomWalk(pCurrSpot);
        vPath.push_back(n);
        pCurrSpot = pCurrSpot->vLinks[n].pTo;

        if(pCurrSpot == pInitSpot || !IsIsolated(pCurrSpot))
            break;
    }

    pCurrSpot = pStarSpot;
    

    for(i = 0; i < vPath.size(); ++i)
    {
        Spot* pNewSpot = pCurrSpot->vLinks[vPath[i]].pTo;
        if(IsIsolated(pNewSpot) || i == vPath.size() - 1)
            pCurrSpot->vLinks[vPath[i]].pPass->bWalled = false;
        pCurrSpot = pNewSpot;
    }

    return true;
}

void WilsonMaze(Maze& mz)
{
    Spot* pInitSpot = mz.vSpots[rand()%mz.vSpots.size()];

    while(WilsonMazeStep(mz, pInitSpot));
}

bool RecursiveMazeStep(std::list<Spot*>& vStack)
{
    if(vStack.empty())
        return false;
    
    Spot* pSpot = vStack.back();

    std::vector<int> vChoices;

    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(i);
    if(vChoices.empty())
    {
        vStack.pop_back();
        return true;
    }

    int n = vChoices[rand()%vChoices.size()];

    pSpot->vLinks[n].pPass->bWalled = false;
    vStack.push_back(pSpot->vLinks[n].pTo);

    return true;
}

void RecursiveMaze(Maze& mz)
{
    std::list<Spot*> vStack;
    vStack.push_back(mz.vSpots[rand()%mz.vSpots.size()]);

    while(RecursiveMazeStep(vStack));
}

bool PrimMazeStep(std::vector<Spot*>& vFrontiers)
{
    if(vFrontiers.empty())
        return false;

    unsigned i;
    int n = rand()%vFrontiers.size();
    Spot* pSpot = vFrontiers[n];
    vFrontiers.erase(vFrontiers.begin() + n);
    pSpot->nState = 2;

    std::vector<Link> vLnkIns;
    for(i = 0; i < pSpot->vLinks.size(); ++i)
        if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pTo->nState == 2)
            vLnkIns.push_back(pSpot->vLinks[i]);

    vLnkIns[rand()%vLnkIns.size()].pPass->bWalled = false;
    
    for(i = 0; i < pSpot->vLinks.size(); ++i)
        if(pSpot->vLinks[i].pTo && pSpot->vLinks[i].pTo->nState == 0)
        {
            pSpot->vLinks[i].pTo->nState = 1;
            vFrontiers.push_back(pSpot->vLinks[i].pTo);
        }

    return true;
}

void PrimMazeInit(std::vector<Spot*>& vFrontiers, Spot* pInitSpot)
{
    pInitSpot->nState = 2;

    unsigned i;
    for(i = 0; i < pInitSpot->vLinks.size(); ++i)
        if(pInitSpot->vLinks[i].pTo)
        {
            pInitSpot->vLinks[i].pTo->nState = 1;
            vFrontiers.push_back(pInitSpot->vLinks[i].pTo);
        }

}

void PrimMaze(Maze& mz)
{
    Spot* pInitSpot = mz.vSpots[rand()%mz.vSpots.size()];

    std::vector<Spot*> vFrontiers;

    PrimMazeInit(vFrontiers, pInitSpot);

    while(PrimMazeStep(vFrontiers));
}

template<class T>
void Shuffle(std::vector<T>& v)
{
    std::vector<T> vShuff;

    while(!v.empty())
    {
        int i = rand()%v.size();
        vShuff.push_back(v[i]);
        v.erase(v.begin() + i);
    }

    v = vShuff;
}



void KruskalMazeInit(Maze& mz, std::vector<KruskalWall>& vWalls)
{
    Spot* pInitSpot = mz.vSpots.at(0);

    pInitSpot->nState = 1;

    std::list<Spot*> vFrontier;
    vFrontier.push_back(pInitSpot);

    while(!vFrontier.empty())
    {
        Spot* pSpot = vFrontier.back();
        vFrontier.pop_back();
        pSpot->nState = 2;

        for(unsigned j = 0; j < pSpot->vLinks.size(); ++j)
        {
            if(pSpot->vLinks[j].pTo && pSpot->vLinks[j].pTo->nState != 2)
            {
                KruskalWall kw;
                kw.l = pSpot->vLinks[j];
                kw.pSpot = pSpot;
                vWalls.push_back(kw);

                if(pSpot->vLinks[j].pTo->nState == 0)
                {
                    pSpot->vLinks[j].pTo->nState = 1;
                    vFrontier.push_back(pSpot->vLinks[j].pTo);
                }
            }
        }
    }

    unsigned j;
    for(j = 0; j < mz.vSpots.size(); ++j)
        mz.vSpots[j]->nState = j;

    Shuffle(vWalls);
}

bool KruskalMazeStep(Maze& mz, std::vector<KruskalWall>& vWalls)
{
    if(vWalls.empty())
        return false;

    KruskalWall kw = vWalls.back();
    vWalls.pop_back();

    if(kw.pSpot->nState == kw.l.pTo->nState)
        return true;
    std::list<Spot*> lsRecolor;
    lsRecolor.push_back(kw.l.pTo);
    while(!lsRecolor.empty())
    {
        Spot* pCurr = lsRecolor.back();
        lsRecolor.pop_back();
        pCurr->nState = kw.pSpot->nState;
        for(unsigned i = 0; i < pCurr->vLinks.size(); ++i)
            if(pCurr->vLinks[i].pPass && !pCurr->vLinks[i].pPass->bWalled)
            if(pCurr->vLinks[i].pTo->nState != kw.pSpot->nState)
                lsRecolor.push_back(pCurr->vLinks[i].pTo);
    }
    kw.l.pPass->bWalled = false;

    return true;
}


void KruskalMaze(Maze& mz)
{
    std::vector<KruskalWall> vWalls;

    KruskalMazeInit(mz, vWalls);

    while(KruskalMazeStep(mz, vWalls));
}

bool HuntKillMazeStep(Maze& mz, Spot*& pSpot)
{
    unsigned i,j, sz;

    std::vector<int> vChoices;

    for(i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(i);
    if(!vChoices.empty())
    {
        int n = vChoices[rand()%vChoices.size()];

        pSpot->vLinks[n].pPass->bWalled = false;
        pSpot = pSpot->vLinks[n].pTo;
        return true;
    }

    std::vector<Link> vHuntLinks;

    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        Spot* pHuntSpot = mz.vSpots[i];
        if(!IsIsolated(pHuntSpot))
            continue;
        for(j = 0; j < pHuntSpot->vLinks.size(); ++j)
            if(pHuntSpot->vLinks[j].pTo && !IsIsolated(pHuntSpot->vLinks[j].pTo))
            {
                Link l;
                l.pPass = pHuntSpot->vLinks[j].pPass;
                l.pTo = pHuntSpot;
                vHuntLinks.push_back(l);
            }
    }

    if(vHuntLinks.empty())
        return false;
    
    int n = rand()%vHuntLinks.size();
    pSpot = vHuntLinks[n].pTo;
    vHuntLinks[n].pPass->bWalled = false;
    return true;
}

void HuntKillMaze(Maze& mz)
{
    Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];
    
    while(HuntKillMazeStep(mz, pSpot));
}

bool GrowingTreeMazeStep(std::vector<Spot*>& vStack)
{
    if(vStack.empty())
        return false;
    
    int n;
    if(rand()%10 == 0)
        n = rand()%vStack.size();
    else
        n = vStack.size() - 1;
    Spot* pSpot = vStack[n];

    std::vector<int> vChoices;

    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(i);

    if(vChoices.empty())
    {
        vStack.erase(vStack.begin() + n);
        return true;
    }

    n = vChoices[rand()%vChoices.size()];

    pSpot->vLinks[n].pPass->bWalled = false;
    vStack.push_back(pSpot->vLinks[n].pTo);

    return true;
}

void GrowingTreeMaze(Maze& mz)
{
    std::vector<Spot*> vStack;
    vStack.push_back(mz.vSpots[rand()%mz.vSpots.size()]);

    while(GrowingTreeMazeStep(vStack));
}

void DivideMazeRec(SquareMaze& mz, Rectangle r, std::list<Rectangle>& v)
{
    if(r.sz.x == 1 || r.sz.y == 1)
        return;

    //if(float(rand())/RAND_MAX < float(r.sz.x) / (r.sz.y + r.sz.x))
    if(float(rand())/RAND_MAX < float(r.sz.y) / (r.sz.y + r.sz.x))
    //if(r.sz.y > r.sz.x)
    //if(rand()%2)
    {
        Point p;
        p.y = rand()%(r.sz.y - 1);
        for(p.x = 0; p.x < r.sz.x; ++p.x)
            mz.mtxSpots[r.p + p].vLinks[3].pPass->bWalled = true;

        p.x = rand()%r.sz.x;
        mz.mtxSpots[r.p + p].vLinks[3].pPass->bWalled = false;

        v.push_back(Rectangle(r.Left(), r.Top(), r.Right(), r.Top() + p.y + 1));
        v.push_back(Rectangle(r.Left(), r.Top() + p.y + 1, r.Right(), r.Bottom()));
    }
    else
    {
        Point p;
        p.x = rand()%(r.sz.x - 1);
        for(p.y = 0; p.y < r.sz.y; ++p.y)
            mz.mtxSpots[r.p + p].vLinks[1].pPass->bWalled = true;

        p.y = rand()%r.sz.y;
        mz.mtxSpots[r.p + p].vLinks[1].pPass->bWalled = false;

        v.push_back(Rectangle(r.Left(), r.Top(), r.Left() + p.x + 1, r.Bottom()));
        v.push_back(Rectangle(r.Left() + p.x + 1, r.Top(), r.Right(), r.Bottom()));
    }
}

void DivideMazeInit(SquareMaze& mz, std::list<Rectangle>& v)
{
    Point p;
    for(p.x = 0; p.x < mz.mtxSpots.GetSize().x; ++p.x)
    for(p.y = 0; p.y < mz.mtxSpots.GetSize().y; ++p.y)
    for(int i = 0; i < 4; ++i)
        if(mz.mtxSpots[p].vLinks[i].pPass)
            mz.mtxSpots[p].vLinks[i].pPass->bWalled = false;

    v.push_back(mz.mtxSpots.GetSize());

}

void DivideMaze(SquareMaze& sq)
{
    std::list<Rectangle> v;
    DivideMazeInit(sq, v);

    while(DivideMazeStep(sq, v));
}

bool DivideMazeStep(SquareMaze& sq, std::list<Rectangle>& v)
{
    std::list<Rectangle> vBuff;
    while(!v.empty())
    {
        DivideMazeRec(sq, v.front(), vBuff);
        v.pop_front();
    }
    v = vBuff;
    return !v.empty();
}

void EllerMazeInit(SquareMaze& sq)
{
    Point p;
    for(p.y = 0; p.y < sq.mtxSpots.GetSize().y; ++p.y)
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        sq.mtxSpots[p].nState = -1;

    p = Point(0,0);
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        sq.mtxSpots[p].nState = p.x;
}

bool EllerMazeStep(SquareMaze& sq, int& nY)
{
    Point p(0, nY);
    if(nY < sq.mtxSpots.GetSize().y - 1)
    {
        // Horizontal passages
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            Point pPrev = Point(p.x - 1, p.y);

            if(sq.mtxSpots[p].nState != sq.mtxSpots[pPrev].nState)
                if(rand()%3)
                {
                    sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
                    int nStateFrom = sq.mtxSpots[p].nState;
                    int nStateTo = sq.mtxSpots[pPrev].nState;

                    Point q(0, p.y);
                    for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
                        if(sq.mtxSpots[q].nState == nStateFrom)
                            sq.mtxSpots[q].nState = nStateTo;
                }
        }
            
        // Vertical 'must have' passages
        std::vector<bool> vToCarve(sq.mtxSpots.GetSize().x, false);
        
        for(int id = 0; id < sq.mtxSpots.GetSize().x; ++id)
        {
            std::vector<int> vPnts;
            for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
                if(sq.mtxSpots[p].nState == id)
                    vPnts.push_back(p.x);
            
            if(vPnts.empty())
                continue;
            
            vToCarve[vPnts[rand()%vPnts.size()]] = true;
        }

        // Vertical passages
        for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            if(vToCarve[p.x] || (rand()%3 == 0))
            {
                sq.mtxSpots[p].vLinks[3].pPass->bWalled = false;
                sq.mtxSpots[Point(p.x, p.y + 1)].nState = sq.mtxSpots[p].nState;
            }
        }

        // Id assignment
        Point q = Point(0, p.y + 1);
        std::set<int> sIds;
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            sIds.insert(q.x);
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            sIds.erase(sq.mtxSpots[q].nState);
        for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
            if(sq.mtxSpots[q].nState == -1)
            {
                sq.mtxSpots[q].nState = *sIds.begin();
                sIds.erase(sIds.begin());
            }
    }
    else if(nY == sq.mtxSpots.GetSize().y - 1)
    {
        // final row iteration
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            Point pPrev = Point(p.x - 1, p.y);

            if(sq.mtxSpots[p].nState != sq.mtxSpots[pPrev].nState)
            {
                sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
                int nStateFrom = sq.mtxSpots[p].nState;
                int nStateTo = sq.mtxSpots[pPrev].nState;

                Point q(0, p.y);
                for(q.x = 0; q.x < sq.mtxSpots.GetSize().x; ++q.x)
                    if(sq.mtxSpots[q].nState == nStateFrom)
                        sq.mtxSpots[q].nState = nStateTo;
            }
        }
    }
    else
        return false;

    ++nY;
    return true;
}

void EllerMaze(SquareMaze& sq)
{
    EllerMazeInit(sq);

    int nY = 0;

    while(EllerMazeStep(sq, nY));
    
}

void BinaryMaze(SquareMaze& sq)
{
    Point p;
    for(p.y = 0; p.y < sq.mtxSpots.GetSize().y; ++p.y)
    for(p.x = 0; p.x < sq.mtxSpots.GetSize().x; ++p.x)
    {
        int n = rand()%2;
        if (((n == 0) || (p.y == 0)) && (p.x != 0))
            sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
        if (((n == 1) || (p.x == 0)) && (p.y != 0))
            sq.mtxSpots[p].vLinks[2].pPass->bWalled = false;
    }
}

bool SidewinderMazeStep(SquareMaze& sq, int& nY)
{
    Point p (0 , nY);
    if(nY > 0)
    {
        int nStart = 0;
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
        {
            if(rand()%3)
                sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
            else
            {
                int n = nStart + rand()%(p.x - nStart);
                sq.mtxSpots[Point(n, p.y)].vLinks[2].pPass->bWalled = false;
                nStart = p.x;
            }
        }
        int n = nStart + rand()%(p.x - nStart);
        sq.mtxSpots[Point(n, p.y)].vLinks[2].pPass->bWalled = false;
    }
    else if(nY == 0)
    {
        for(p.x = 1; p.x < sq.mtxSpots.GetSize().x; ++p.x)
            sq.mtxSpots[p].vLinks[0].pPass->bWalled = false;
    }
    else
        return false;

    --nY;
    return true;
}

void SidewinderMaze(SquareMaze& sq)
{
    int nY = sq.mtxSpots.GetSize().y - 1;
    while(SidewinderMazeStep(sq, nY));
}


