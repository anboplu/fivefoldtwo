#include <stdlib.h>

#include <map>
#include <vector>

#include "GuiSdl.h"
#include "Maze.h"

#include <time.h>
#include <iostream>

#define TRUE 1
#define FALSE 0

using namespace Gui;

int main(int argc, char *argv[])
{
    srand( (unsigned)time( NULL ));

	SDL_Event event;

    bool bTrue = true;
    bool bExit = false;
    
    try
    {
        Rectangle sBound = Rectangle(0, 0, 1000, 700);

        SP<SdlGraphicalInterface> pGraph = new SdlGraphicalInterface(sBound.sz);

        SP< GraphicalInterface<IndexImg> > pGr = 
        new SimpleGraphicalInterface<SdlImage*>(pGraph);

        pGr->DrawRectangle(sBound, Color(255,255,255), true);

        SDL_WM_SetCaption("Mazing", NULL);

        Uint32 nTimer = SDL_GetTicks();

        Size szSq = Size(300, 150);
        //Size szSq = Size(100, 100);
        SquareMaze sq(szSq);
        CubeMaze   cb(Size(20, 20), 5);
        //sq.mtxSpots[Point()].vLinks[1].pPass->bWalled = false;

        Maze mz;
        sq.GetMaze(mz);

        Spot* pSpot = mz.vSpots[rand()%mz.vSpots.size()];

        //AldousBroderMaze(mz);
        //WilsonMaze(mz);
        //PrimMaze(mz);
        //KruskalMaze(mz);
        //RecursiveMaze(mz);
        //HuntKillMaze(mz);
        //GrowingTreeMaze(mz);
        //DivideMaze(sq);
        //EllerMaze(sq);
        //BinaryMaze(sq);
        //SidewinderMaze(sq);


        //std::vector<Spot*> vFrontiers;
        //PrimMazeInit(vFrontiers, pSpot);

        //std::vector<KruskalWall> vWalls;
        //KruskalMazeInit(mz, vWalls);
                
        //std::list<Spot*> vStack;
        //vStack.push_back(pSpot);

        //std::vector<Spot*> vStack;
        //vStack.push_back(pSpot);

        std::list<Rectangle> v;
        DivideMazeInit(sq, v);

        //int nY = 0;
        //EllerMazeInit(sq);

        //int nY = sq.mtxSpots.GetSize().y - 1;

        int nAtOnce = 1;
        int nLvl = 0;

        while(!bExit) {
    		
		    if(SDL_GetTicks() - nTimer > 200)
            {
                nTimer = SDL_GetTicks();
                //pCurrControl->Update();

                pGr->DrawRectangle(sBound, Color(255,255,255), false);
                
                for(int k = 0; k < nAtOnce; ++k)
                {
                    //AldousBroderMazeStep(pSpot);
                    //WilsonMazeStep(mz, pSpot);
                    //PrimMazeStep(vFrontiers);
                    //KruskalMazeStep(mz, vWalls);
                    //RecursiveMazeStep(vStack);
                    //HuntKillMazeStep(mz, pSpot);
                    //GrowingTreeMazeStep(vStack);
                    DivideMazeStep(sq, v);
                    //EllerMazeStep(sq, nY);
                    //SidewinderMazeStep(sq, nY);
                }
                
                //Draw(sq, pGr, Point(10, 10), Size(6, 6));
                Draw(sq, pGr, Point(10, 10), Size(3, 3));
                //Draw(cb, pGr, Point(10, 10), Size(10, 10), nLvl);
                
                /*
                for(int i = 0; i < 20000; ++i)
                {
                    Point p = Point(rand()%sBound.sz.x, rand()%sBound.sz.y);
                    pGr->DrawRectangle(Rectangle(p, Size(1,1)), Color(), false); 
                }
                */

                pGr->RefreshAll();
            }
            
            if( SDL_PollEvent( &event ) )
            {
			    int i = event.type;
                
                if(event.type == SDL_QUIT)
                    break;
                
			    switch( event.type ){
                    case SDL_QUIT:
                        bExit = true;
                        break;
                    case SDL_KEYDOWN:
                        //pCurrControl->OnKey(event.key.keysym.sym, false);
                        
                        switch(event.key.keysym.sym) {
                            case SDLK_EQUALS:
                              std::cout << "Global: " << nGlobalSuperMegaCounter << "\n"; 
                              break;
                            case SDLK_ESCAPE:
                              bExit = true;
                              break;
                            case SDLK_DOWN:
                              ++nLvl;
                              if(unsigned(nLvl) >= cb.cbSpots.size())
                                  --nLvl;
                              break;
                            case SDLK_UP:
                              --nLvl;
                              if(nLvl < 0)
                                  ++nLvl;
                            default:
                              break;
                        }
                        break;

                    case SDL_KEYUP:
                        //pCurrControl->OnKey(event.key.keysym.sym, true);
                    case SDL_MOUSEBUTTONDOWN:
                        //wp.OnMouse();
                        break;
                    default:
                        break;
			    }
		    }
	    }
    }
    catch(MyException& me)
    {
        std::cout << me.GetDescription(true) << "\n";
    }
    //catch(...)
    //{
    //    std::cout << "Unknown error!\n";
    //}

    if(nGlobalSuperMegaCounter != 0)
        std::cout << "Memory Leak: " << nGlobalSuperMegaCounter << "\n"; 

    return 0;
}
