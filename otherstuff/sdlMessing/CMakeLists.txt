# The name of our project is "MAZE". CMakeLists files in this project can 
# refer to the root source directory of the project as ${MAZE_SOURCE_DIR} and 
# to the root binary directory of the project as ${MAZE_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 
project (MAZE)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

if(UNIX)
    include_directories (${MAZE_SOURCE_DIR}/include_ln)
else(UNIX)
    include_directories (${MAZE_SOURCE_DIR}/include)
endif(UNIX)

link_directories (${MAZE_SOURCE_DIR})

add_executable(MAZE main.cpp GuiGen.h GuiGen.cpp GuiSdl.cpp GuiSdl.h SmartPointer.h SmartPointer.cpp Maze.h Maze.cpp)

target_link_libraries(MAZE SDLmain SDL SDL_image SDL_mixer)
