#ifndef MAZE_ALREADY_INCLUDED_MAZE_0816
#define MAZE_ALREADY_INCLUDED_MAZE_0816

#include "GuiGen.h"
#include <map>

using namespace Gui;

template<class T> 
class Matrix
{
    Size sz;
    std::vector<T> v;
public:
    Matrix(Size sz_):sz(sz_), v(sz_.x * sz_.y){}

    T& operator [](Point p){return v[p.x + p.y * sz.x];}
    T& at(Point p){return v.at(p.x + p.y * sz.x);}

    Size GetSize(){return sz;}
};

struct Spot;
struct Link;
struct Pass;

struct Spot
{
    std::vector<Link> vLinks;

    int nState;

    Spot():nState(0){}
};

struct Link
{
    Pass* pPass;
    Spot* pTo;

    Link():pPass(0), pTo(0){}
};

struct Pass
{
    bool bWalled;

    Pass(bool bWalled_):bWalled(bWalled_){}
};

struct Maze
{
    std::vector<Spot*> vSpots;
};

struct SquareMaze
{
    std::vector<Pass*> vPasses;
    Matrix<Spot> mtxSpots;

    SquareMaze(Size sz);

    void GetMaze(Maze& m);

    ~SquareMaze()
    {
        for(unsigned i = 0; i < vPasses.size(); ++i)
            delete vPasses[i];
    }
};


void Draw(SquareMaze& sq, SP< GraphicalInterface<IndexImg> > pGr, Point pOffset, Size szElement);

struct CubeMaze
{
    std::vector<Pass*> vPasses;
    std::vector<Matrix<Spot> > cbSpots;
    Size sz;

    CubeMaze(Size sz_, int nHeight);

    void GetMaze(Maze& m);

    ~CubeMaze()
    {
        for(unsigned i = 0; i < vPasses.size(); ++i)
            delete vPasses[i];
    }
};


void Draw(CubeMaze& sq, SP< GraphicalInterface<IndexImg> > pGr, Point pOffset, Size szElement, int nLvl);

struct KruskalWall
{
    Spot* pSpot;
    Link l;
};

void AldousBroderMazeStep(Spot*& pSpot);
void AldousBroderMaze(Maze& mz);

bool WilsonMazeStep(Maze& mz, Spot* pInitSpot);
void WilsonMaze(Maze& mz);

bool RecursiveMazeStep(std::list<Spot*>& vStack);
void RecursiveMaze(Maze& mz);

void PrimMazeInit(std::vector<Spot*>& vFrontiers, Spot* pInitSpot);
bool PrimMazeStep(std::vector<Spot*>& vFrontiers);
void PrimMaze(Maze& mz);

void KruskalMazeInit(Maze& mz, std::vector<KruskalWall>& vWalls);
bool KruskalMazeStep(Maze& mz, std::vector<KruskalWall>& vWalls);
void KruskalMaze(Maze& mz);

bool HuntKillMazeStep(Maze& mz, Spot*& pSpot);
void HuntKillMaze(Maze& mz);

bool GrowingTreeMazeStep(std::vector<Spot*>& vStack);
void GrowingTreeMaze(Maze& mz);

void DivideMazeInit(SquareMaze& sq, std::list<Rectangle>& v);
bool DivideMazeStep(SquareMaze& sq, std::list<Rectangle>& v);
void DivideMaze(SquareMaze& sq);

void EllerMazeInit(SquareMaze& sq);
bool EllerMazeStep(SquareMaze& sq, int& nY);
void EllerMaze(SquareMaze& sq);

void BinaryMaze(SquareMaze& sq);

bool SidewinderMazeStep(SquareMaze& sq, int& nY);
void SidewinderMaze(SquareMaze& sq);



#endif // MAZE_ALREADY_INCLUDED_MAZE_0816