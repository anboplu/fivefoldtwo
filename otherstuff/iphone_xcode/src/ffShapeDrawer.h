#ifndef FF_SHAPE_DRAWER
#define FF_SHAPE_DRAWER

#include<vector>
#include<list>
using namespace std;

struct ffShapeDrawerPair
{
	float x,y;
	ffShapeDrawerPair()
	{
		x = y = 0;
	}
	ffShapeDrawerPair(float _x, float _y)
	{
		x = _x;
		y = _y;
	}
};
void drawPentagon(bool isFlipped, float _x, float _y, vector<bool> drawSides, float _iter = -1, float radius = 50, int sides = 5);
void drawPentagonPulse(bool isFlipped, float _x, float _y,  list<ffShapeDrawerPair> & pulsePairs, int nFrame, vector<bool> drawSides, float radius, bool bExit = false, int sides = 5);
void drawGuy(float _x, float _y, float _iter);

#endif