#include "ofGraphics.h"
#include "ffShapeDrawer.h"
#include "ofTypes.h"
#include "ofMath.h"
#include <vector>
#include <cmath>
#include <iostream>

void drawPentagon(bool isFlipped, float _x, float _y, vector<bool> drawSides,  float _iter, float radius, int sides)
{
	if(drawSides.size() != 5)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;	//should be pi

	//create our style object
	ofStyle myStyle;
	myStyle.bFill = true;
	myStyle.blending = true;

	//fade is overloaded, -1 is default value and draws pentagon at it's lightest visible setting
	//(0,1) is used for continuous fade amounts
	//0,1,2,3... are used for iterative fading
	myStyle.color.a = 170 - _iter*16;
	if(_iter == -1 || myStyle.color.a < 32)
		myStyle.color.a = 32;
	if(_iter > 0 && _iter < 1)
	{
		myStyle.color.a = 255*_iter - 64;
		
	}

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));


	//transform to the position we want
	ofPushMatrix();
	ofTranslate(_x,_y);

	ofPushStyle();
	ofSetStyle(myStyle);
	//draw a filled poylgon

    //glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ofBeginShape();
	for(int i = 0; i <= sides; i++)
	{
		ofVertex(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	}
	ofEndShape();
	ofPopStyle();

	myStyle.color.a += 48;
	myStyle.color.g = 170;
	myStyle.color.r = 170;
	myStyle.bFill = false;
	myStyle.lineWidth = 2;
	myStyle.smoothing = true;
	ofPushStyle();
	ofSetStyle(myStyle);
	ofBeginShape();
	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
			ofLine(pts[i].x,pts[i].y, pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y);
	}
	ofEndShape();
	ofPopStyle();

	myStyle.color.a += 32;
	//myStyle.color.g = 255;
	//myStyle.color.r = 255;
	myStyle.bFill = true;
	myStyle.circleResolution = 5;
	ofPushStyle();
	ofSetStyle(myStyle);
	ofBeginShape();
	for(int i = 0; i < sides; i++)
	{
		ofCircle(pts[(i)%(sides)].x, pts[(i)%(sides)].y,2);
	}
	ofEndShape();
	ofPopStyle();


	ofPopMatrix();
}

void drawGuy(float _x, float _y, float _iter)
{
	//_iter *= 5;
	float phi = (_iter+100)/10.0;
	float theta = _iter/101.0;	//so it is relatively prime with 100.0
	float tau = _iter/32.0;
	//get a point of the sphere and convert it to xyz
	ofPoint pt;
	pt.z = cos(theta);
	pt.x = pt.y = sin(theta);
	pt.y *= sin(phi);
	pt.x *= cos(phi);
	float bright = 80 * sin(tau);
	ofStyle myStyle;
	pt *= bright;
	myStyle.blending = true;
	myStyle.color.r = myStyle.color.b = myStyle.color.g = 180-bright;
	myStyle.color.r += pt.x;
	myStyle.color.g += pt.y;
	myStyle.color.b += pt.z;
	myStyle.color.r = 255 - myStyle.color.r;
	myStyle.color.g = 255 - myStyle.color.g;
	myStyle.color.b = 255 - myStyle.color.b;

	ofPushMatrix();
	ofTranslate(_x,_y);
	ofRotateZ(45);
	ofPushStyle();
	float steps = 15;
	for(int i = 0; i < steps; i++)
	{
		myStyle.color.a = i*255.0/steps;
		ofSetStyle(myStyle);
		ofRect(-(steps-i)/2.0,-(steps-i)/2.0,(steps-i),(steps-i));
		//ofCircle(0,0,(steps-i));
	}

	ofPopStyle();
	ofPopMatrix();
}

template<class T>
struct MaxTracker
{
    bool bInit;
    T val;

    MaxTracker():bInit(false){}
    MaxTracker(T t):bInit(true), val(t){}

    void Boom(T t)
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
        }
    }
};

void drawPentagonPulse(bool isFlipped, float _x, float _y,  list<ffShapeDrawerPair> & pulsePairs, int nFrame, vector<bool> drawSides, float radius, bool bExit, int sides)
{
	float pulseSpeed = 1/3.0; //1 iteration per 100 cycles;
	float startingMaxHeight = 255 - 30;
	float heightChangeSlope = 3;
	//float heightChangeSlope = 1./10;
	float a = 30;
    MaxTracker<float> mt(0);
    for(list<ffShapeDrawerPair>::iterator itr = pulsePairs.begin(), etr = pulsePairs.end();
        itr != etr; ++itr)
	{
		//calculate the height
		//.x is frames elapsed since pulse triggered
		float h = startingMaxHeight - (nFrame - itr->x) * heightChangeSlope;
		//float h = startingMaxHeight*(1./((nFrame - itr->x) * heightChangeSlope + 1) );
        if(h < startingMaxHeight/8)
            h = startingMaxHeight/8;
		//calculate the distance from pulse
		//.y is iteration from pulse triggering source
		float d = abs(itr->y - pulseSpeed*(nFrame - itr->x))/2.0;

		//calculate the alpha
		mt.Boom(h*exp(-(d*d)));
        //if(pulsePairs[i].y == 0)
        //    a = startingMaxHeight;
	}
    a += mt.val;

	if(drawSides.size() != 5)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;	//should be pi

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));

	//create our style object
	ofStyle myStyle;

	//transform to the position we want
	ofPushMatrix();
	ofTranslate(_x,_y);

	ofPushStyle();
	myStyle.bFill = true;
	myStyle.blending = true;
	myStyle.color.a = a;
	myStyle.color.r = myStyle.color.g = myStyle.color.b = 210;
	ofSetStyle(myStyle);

    ofBeginShape();
	for(int i = 0; i <= sides; i++)
	{
		ofVertex(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	}
	ofEndShape();
	ofPopStyle();

	ofPushStyle();
	myStyle.color.r = 240;
	myStyle.color.g = 200;
	myStyle.color.b = 120;
	myStyle.bFill = false;
	myStyle.lineWidth = 3;
	myStyle.smoothing = true;
	ofSetStyle(myStyle);

	ofBeginShape();
	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
			ofLine(pts[i].x,pts[i].y, pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y);
	}
	ofEndShape();
	ofPopStyle();

	myStyle.color.r = myStyle.color.g = myStyle.color.b = 255;
	myStyle.bFill = true;
	myStyle.circleResolution = 5;
	ofPushStyle();
	ofSetStyle(myStyle);
	ofBeginShape();
	for(int i = 0; i < sides; i++)
	{
		ofCircle(pts[(i)%(sides)].x, pts[(i)%(sides)].y,2);
	}
	ofEndShape();
	ofPopStyle();

	if(bExit)
	{
		ofPushStyle();
		myStyle.bFill = true;
		myStyle.blending = false;
		myStyle.color.a = 255; // should not need this but just in case
		myStyle.color.r = myStyle.color.g = myStyle.color.b = 255;
		ofSetStyle(myStyle);
		//draw the hole
		ofRect(-radius/4,-radius/2,radius/2,radius);


		myStyle.color.r = myStyle.color.g = myStyle.color.b = 140;
		ofSetStyle(myStyle);
		//draw the door
		ofBeginShape();
		ofVertex(-radius/4.,radius/2.);
		ofVertex(-radius/4.+radius/2.-radius/20.,radius/2.+radius/10.);
		ofVertex(-radius/4.+radius/2.-radius/20.,radius/2.-radius+radius/10.);
		ofVertex(-radius/4.,radius/2.-radius);
		ofEndShape();
		ofPopStyle();
	}    
    
    ofPopMatrix();
}