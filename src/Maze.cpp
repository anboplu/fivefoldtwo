#include "Maze.h"
#include "ffShapeDrawer.h"
#include "ofGraphics.h"
#include "ffShapeDrawer.h"
#include "ofTypes.h"
#include "ofMath.h"
#include "ofMain.h"
#include "ofTrueTypeFont.h"

void fPoint::Normalize(float f/* = 1*/)
{
    if(x == 0 && y == 0)
        return;
    
    (*this) *= f/Length();
}

FiveCoord& operator += (FiveCoord& f1, FiveCoord f2)
{
    for(int i = 0; i < 5; ++i)
        f1.v[i] += f2.v[i];
    return f1;
}

FiveCoord operator + (FiveCoord f1, FiveCoord f2)
{
    FiveCoord t = f1;
    t += f2;
    return t;
}


bool SameSide(fPoint p1, fPoint p2, fPoint l1, fPoint l2)
{
    fPoint p = p2 - p1;
    fPoint l = l2 - l1;

    float a1 = p.y;
    float b1 = -p.x;
    float c1 = p1.x*a1 + p1.y*b1;
    float a2 = l.y;
    float b2 = -l.x;
    float c2 = l1.x*a2 + l1.y*b2;

    if(abs((b1*a2- b2*a1)) < .0001F)
        return true;

    float y =  ( c1*a2 - c2*a1 ) / ( b1*a2- b2*a1 );
    float x = -( c1*b2 - c2*b1 ) / ( b1*a2- b2*a1 );

    fPoint c(x, y);

    if( (c - p1).Length() < p.Length() )
        if( (c - p2).Length() < p.Length() )
            return false;
    return true;
}

std::ostream& operator << (std::ostream& ostr, FiveCoord f)
{
    for(int i = 0; i < 5; ++i)
        ostr << f.v[i] << " ";
    return ostr;
}

std::istream& operator >> (std::istream& istr, FiveCoord& f)
{
    for(int i = 0; i < 5; ++i)
        istr >> f.v[i];
    return istr;
}

int FiveCoord::GetSum() const
{
    return v[0] + v[1] + v[2] + v[3] + v[4];
}


fPoint GetDir(double d)
{
    return fPoint(float(cos(HALF_PI + d)), float(-sin(HALF_PI + d)));
}

fPoint GetDir(int n)
{
    return GetDir(TWO_PI/5 * n);
}

float Dst(fPoint f1, fPoint f2)
{
    return (f1 - f2).Length();
}

Pentagon::Pentagon(FiveCoord fc_, float fRad_, bool bFlipped_, WallDef vWalls_)
    :fc(fc_), fRad(fRad_), bFlipped(bFlipped_), vWalls(vWalls_), nAge(0)
{
    fCenter = fPoint();
    int i;
    for(i = 0; i < 5; ++i)
        fCenter += GetDir(i)*float(fc[i])*fRad*2*cos(TWO_PI/10);
    for(i = 0; i < 5; ++i)
        vVert.push_back(fCenter + GetDir(i)*fRad * float(bFlipped ? -1 : 1) );
    for(i = 0; i < 5; ++i)
    {
        Side sd;
        sd.p1 = vVert[i];
        sd.p2 = vVert[(i + 1)%5];
        sd.fcDelta[(i + 3)%5] = (bFlipped ? 1 : -1);
        vSides.push_back(sd);
    }
}

bool Pentagon::CloseToVertice(fPoint p)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(Dst(p, vVert[i]) < (fRad / 10))
            return true;
    return false;
}

bool Pentagon::CrossingWall(fPoint p)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(vWalls[i] && !SameSide(fCenter, p, vSides[i].p1, vSides[i].p2))
            return true;
    return false;
}


FiveCoord Pentagon::CrossSide(fPoint p)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(!SameSide(fCenter, p, vSides[i].p1, vSides[i].p2))
            return vSides[i].fcDelta;
    return FiveCoord();
}

void Game::Draw()
{
    if(!bExist)
        return;
    
    pOffset = fPoint(ofGetWidth()/2, ofGetHeight()/2);
    ofPushMatrix();
	ofTranslate(pOffset.x, pOffset.y);
    ofPushMatrix();
	ofTranslate(-pGuy.x, -pGuy.y);

    int n = ofGetFrameNum();

    for(std::list<Pentagon>::iterator itr = lsPath.begin(), etr = lsPath.end(); itr != etr; ++itr)
    {
        Pentagon& p = *itr;
        drawPentagonPulse(!p.bFlipped, p.fCenter.x, p.fCenter.y, p.lsPulse, n, p.vWalls.v, p.fRad, itr->fc == fcExit);
    }

    drawGuy(pGuy.x, pGuy.y, ofGetFrameNum());

    ofPopMatrix();
    ofPopMatrix();
}

void Game::Move(fPoint fDir)
{
    if(!bExist)
        return;

    fDir.Normalize(fSpeed);

    fPoint pNew = pGuy + fDir;

    Pentagon& p = lsPath.back();

    if(p.CloseToVertice(pNew) || p.CrossingWall(pNew))
        return;

    pGuy = pNew;

    FiveCoord fc = p.CrossSide(pNew);
    if(!(fc == FiveCoord()))
        Move(p.fc + fc);
}

void Game::Move(FiveCoord fc)
{
    Pentagon& p = lsPath.back();

    std::list<Pentagon>::iterator itr = lsPath.begin(), etr = lsPath.end();
    bool bExit = false;
    for(; itr != etr; ++itr)
    {
        if(itr->fc == fc)
        {
            itr->nAge = 0;
            lsPath.push_back(*itr);
            lsPath.erase(std::list<Pentagon>::iterator(itr));
            bExit = true;
            break;
        }
    }

    if(!bExit)
        lsPath.push_back(Pentagon(fc, p.fRad, !p.bFlipped, mpLevel[fc]));

    //for(itr = lsPath.begin(); itr != etr; ++itr)
    //    ++itr->nAge;
    
    MarkDistances();

    if(fc == fcExit)
        bFinished = true;
}

void Game::MarkDistances()
{
    map<FiveCoord, Pentagon*> mpPath;
    for(list<Pentagon>::iterator itr = lsPath.begin(), etr = lsPath.end(); itr != etr; ++itr)
    {
        mpPath[itr->fc] = &(*itr);
        itr->nAge = -1;
    }

    lsPath.back().nAge = 0;

    list<FiveCoord> lsRecursive;
    lsRecursive.push_back(lsPath.back().fc);

    while(!lsRecursive.empty())
    {
        FiveCoord fc = lsRecursive.front();
        lsRecursive.pop_front();
        Pentagon* pP = mpPath[fc];
        for(int i = 0; i < 5; ++i)
        {
            if(pP->vWalls[i])
                continue;
            Side s = pP->vSides[i];
            FiveCoord fcNew = s.fcDelta + fc;
            
            map<FiveCoord, Pentagon*>::iterator itr = mpPath.find(fcNew);
            if(itr != mpPath.end() && itr->second->nAge == -1)
            {
                itr->second->nAge = pP->nAge + 1;
                lsRecursive.push_back(fcNew);
            }
        }
    }
}

void Game::Pulse()
{
    for(list<Pentagon>::iterator itr = lsPath.begin(), etr = lsPath.end(); itr != etr; ++itr)
    {
        itr->lsPulse.push_back(ffShapeDrawerPair(ofGetFrameNum(), itr->nAge));
        if(itr->lsPulse.size() > 10)
            itr->lsPulse.pop_front();
    }
}



Game::Game(): bFinished(false)

{
    bExist = false;
        
    pGuy = fPoint(0,0);
    fSpeed = 4;

    /*
    //vector<bool> v (5, true);
    vector<bool> v (5, false);
    //v[2] = false;
    FiveCoord fc;
    Pentagon p = Pentagon(fc, 50.F, false, v);
    p.nAge = 1;
    lsPath.push_back(p);
    mpLevel[fc] = v;
    
    fc[0] = -1;
    v[1] = false;
    mpLevel[fc] = v;

    v = vector<bool> (5, true);
    v[1] = false;
    v[0] = false;
    fc[4] = 1;
    mpLevel[fc] = v;

    v = vector<bool> (5, true);
    v[0] = false;
    v[1] = false;
    fc[3] = -1;
    mpLevel[fc] = v;
    */
}

bool IsWalled(Pass* pPass)
{
    return (!pPass) || pPass->bWalled;
}

bool IsIsolated(Spot* pSpot)
{
    unsigned j;
    for(j = 0; j < pSpot->vLinks.size(); ++j)
        if(!IsWalled(pSpot->vLinks[j].pPass))
            break;
    return j == pSpot->vLinks.size();
}

bool RecursiveMazeStep(std::list<Spot*>& vStack)
{
    if(vStack.empty())
        return false;
    
    Spot* pSpot = vStack.back();

    std::vector<int> vChoices;

    for(unsigned i = 0, sz = pSpot->vLinks.size(); i < sz; ++i)
        if(pSpot->vLinks[i].pTo && IsIsolated(pSpot->vLinks[i].pTo))
            vChoices.push_back(i);
    if(vChoices.empty())
    {
        vStack.pop_back();
        return true;
    }

    int n = vChoices[rand()%vChoices.size()];

    pSpot->vLinks[n].pPass->bWalled = false;
    vStack.push_back(pSpot->vLinks[n].pTo);

    return true;
}

void RecursiveMaze(Maze& mz)
{
    std::list<Spot*> vStack;
    vStack.push_back(mz.vSpots[rand()%mz.vSpots.size()]);

    while(RecursiveMazeStep(vStack));
}


Spot* PentaMaze::GetSpot(FiveCoord fc)
{
    std::map<FiveCoord, Spot*>::iterator itr =
        mp.find(fc);
    if(itr == mp.end())
        return 0;
    return itr->second;
}

PentaMaze::PentaMaze(FiveCoord fcBnd) 
{
    std::vector<FiveCoord> vC;
    
    FiveCoord fc;
    for(fc[0] = -fcBnd[0]; fc[0] <= fcBnd[0]; ++fc[0])
    for(fc[1] = -fcBnd[1]; fc[1] <= fcBnd[1]; ++fc[1])
    for(fc[2] = -fcBnd[2]; fc[2] <= fcBnd[2]; ++fc[2])
    for(fc[3] = -fcBnd[3]; fc[3] <= fcBnd[3]; ++fc[3])
    for(fc[4] = -fcBnd[4]; fc[4] <= fcBnd[4]; ++fc[4])
    {
        if((fc.GetSum() == 0 || fc.GetSum() == -1) && (GetSpot(fc) == 0))
        {
            Spot* pS = new Spot();
            pS->vLinks.resize(5);
            mp[fc] = pS;

            vC.push_back(fc);
        }
    }

    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
    {
        FiveCoord fc = itr->first;
        Spot* pS = itr->second;
        for(int i = 0; i < 5; ++i)
        {
            FiveCoord fc_new = fc;
            if(fc.GetSum() == 0)
                --fc_new[i];
            else
                ++fc_new[i];
            Spot* pNew = GetSpot(fc_new);
            if(pNew)
            {
                pS->vLinks[i].pTo = pNew;
                pNew->vLinks[i].pTo = pS;

                Pass* pPass = new Pass(true);
                v.push_back(pPass);
                
                pS->vLinks[i].pPass = pPass;
                pNew->vLinks[i].pPass = pPass;
            }
        }
    }

    Maze mz;
    GetMaze(mz);
    RecursiveMaze(mz);
    //WilsonMaze(mz);

    //std::list<Spot*> vStack;
    //vStack.push_back(mp[FiveCoord()]);

    //for(int i = 0; i < 30; ++i)
    //    RecursiveMazeStep(vStack);

    fcExit = vC[rand()%vC.size()];

}

void PentaMaze::GetMaze(Maze& mz)
{
    mz.vSpots.clear();
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
        mz.vSpots.push_back(itr->second);
}


PentaMaze::~PentaMaze()
{
    for(unsigned i = 0; i < v.size(); ++i)
        delete v[i];
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
        delete itr->second;
}

void PentaMaze::InitLevel(std::map<FiveCoord, WallDef>& mpLevel)
{
    mpLevel.clear();
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
    {
        Spot* pS = itr->second;
        vector<bool> vWall(5, true);
        for(int i = 0; i < 5; ++i)
        {
            int n = (i+3)%5;
            if(pS->vLinks[n].pPass && !pS->vLinks[n].pPass->bWalled)
                vWall[i] = false;
        }
        mpLevel[itr->first] = vWall;
    }
}

int RandomWalk(Spot* pSpot)
{
    int sz = pSpot->vLinks.size();
    int n;

    while(true)
    {
        /*
        if(sz == 4)
        {
            int k = rand()%22;
            if(k < 10)
                n = 0;
            else if(k < 20)
                n = 1;
            else if(k == 20)
                n = 2;
            else
                n = 3;
        }
        else if(sz == 6)
        {
            int k = rand()%402;
            if(k < 100)
                n = 0;
            else if(k < 200)
                n = 1;
            else if(k < 300)
                n = 2;
            else if(k < 400)
                n = 3;
            else if(k == 400)
                n = 4;
            else
                n = 5;
        
        }
        else
        if(sz == 5)
        {
            int k = rand()%311;
            if(k < 100)
                n = 0;
            else if(k < 200)
                n = 4;
            else if(k < 300)
                n = 2;
            else if(k < 310)
                n = 3;
            else// if(k == 310)
                n = 1;
        
        }
        else
        */
            n = rand()%sz;

        if(pSpot->vLinks[n].pPass)
            break;
    }

    return n;
}


bool WilsonMazeStep(Maze& mz, Spot* pInitSpot)
{
    std::vector<Spot*> vIsolated;
    
    unsigned i;
    for(i = 0; i < mz.vSpots.size(); ++i)
    {
        if(mz.vSpots[i] == pInitSpot)
            continue;
        
        if(IsIsolated(mz.vSpots[i]))
            vIsolated.push_back(mz.vSpots[i]);
    }

    if(vIsolated.empty())
        return false;

    Spot* pCurrSpot = vIsolated[rand()%vIsolated.size()];
    Spot* pStarSpot = pCurrSpot;
    std::vector<int> vPath;

    while(true)
    {
        int n = RandomWalk(pCurrSpot);
        vPath.push_back(n);
        pCurrSpot = pCurrSpot->vLinks[n].pTo;

        if(pCurrSpot == pInitSpot || !IsIsolated(pCurrSpot))
            break;
    }

    pCurrSpot = pStarSpot;
    

    for(i = 0; i < vPath.size(); ++i)
    {
        Spot* pNewSpot = pCurrSpot->vLinks[vPath[i]].pTo;
        if(IsIsolated(pNewSpot) || i == vPath.size() - 1)
            pCurrSpot->vLinks[vPath[i]].pPass->bWalled = false;
        pCurrSpot = pNewSpot;
    }

    return true;
}

void WilsonMaze(Maze& mz)
{
    Spot* pInitSpot = mz.vSpots[rand()%mz.vSpots.size()];

    while(WilsonMazeStep(mz, pInitSpot));
}