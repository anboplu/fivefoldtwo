#include <stdlib.h>

#include <map>
#include <set>
#include <list>
#include <vector>
#include <math.h>

#include <time.h>
#include <iostream>

#include "ofConstants.h"
#include "ffShapeDrawer.h"

using namespace std;

struct fPoint
{
    float x,y;

    fPoint():x(0), y(0){}
    fPoint(float x_, float y_):x(x_), y(y_){}

    void Normalize(float f = 1);

    float Length(){return sqrt(x*x + y*y);}
};

inline fPoint& operator += (fPoint& f1, const fPoint& f2){f1.x += f2.x; f1.y += f2.y; return f1;}
inline fPoint& operator -= (fPoint& f1, const fPoint& f2){f1.x -= f2.x; f1.y -= f2.y; return f1;}
inline fPoint operator + (const fPoint& f1, const fPoint& f2){return fPoint(f1) += f2;}
inline fPoint operator - (const fPoint& f1, const fPoint& f2){return fPoint(f1) -= f2;}
inline bool operator == (const fPoint& f1, const fPoint& f2){return (f1.x == f2.x) && (f1.y == f2.y);}
inline bool operator != (const fPoint& f1, const fPoint& f2){return (f1.x != f2.x) || (f1.y != f2.y);}
inline fPoint& operator /= (fPoint& f1, float f){f1.x /= f; f1.y /= f; return f1;}
inline fPoint& operator *= (fPoint& f1, float f){f1.x *= f; f1.y *= f; return f1;}
inline fPoint operator / (const fPoint& f1, float f){return fPoint(f1) /= f;}
inline fPoint operator * (const fPoint& f1, float f){return fPoint(f1) *= f;}

bool SameSide(fPoint p1, fPoint p2, fPoint l1, fPoint l2);

struct FiveCoord
{
    std::vector<int> v;
    FiveCoord():v(5, 0){}
    FiveCoord(int a, int b, int c, int d, int e){v.push_back(a);v.push_back(b);v.push_back(c);v.push_back(d);v.push_back(e);}
    int& operator[](int i){return v.at(i);}
    bool operator < (const FiveCoord& f) const {return v < f.v;}
    bool operator == (const FiveCoord& f) const {return v == f.v;}
    int GetSum() const;
};

std::ostream& operator << (std::ostream& ostr, FiveCoord f);
std::istream& operator >> (std::istream& istr, FiveCoord& f);

FiveCoord& operator += (FiveCoord& f1, FiveCoord f2);

fPoint GetDir(int n);

struct Side
{
    fPoint p1;
    fPoint p2;
    FiveCoord fcDelta;
};

struct WallDef
{
    vector<bool> v;

    WallDef():v(5, false){}
    WallDef(vector<bool> v_):v(v_){}

    bool operator[](unsigned n){return v.at(n);}
    void Set(unsigned n, bool b){v.at(n) = b;}
};

struct Pentagon
{
    FiveCoord fc;
    float fRad;
    bool bFlipped;
    WallDef vWalls;

    int nAge;
    list<ffShapeDrawerPair> lsPulse;
    
    vector<fPoint> vVert;
    vector<Side> vSides;
    fPoint fCenter;

    Pentagon(FiveCoord fc_, float fRad_, bool bFlipped_, WallDef vWalls_);
    bool CloseToVertice(fPoint p);
    bool CrossingWall(fPoint p);
    FiveCoord CrossSide(fPoint p);
};

struct Game
{
    fPoint pOffset;

    FiveCoord fcExit;
    bool bFinished;

    map<FiveCoord, WallDef> mpLevel;
    list<Pentagon> lsPath;

    fPoint pGuy;
    float fSpeed;

    void Draw();
    void Move(fPoint fDir);

    void Move(FiveCoord fc);

    void MarkDistances();
    void Pulse();

    bool bExist;

    Game();
};

struct Spot;
struct Link;
struct Pass;

struct Spot
{
    std::vector<Link> vLinks;

    int nState;

    Spot():nState(0){}
};

struct Link
{
    Pass* pPass;
    Spot* pTo;

    Link():pPass(0), pTo(0){}
};

struct Pass
{
    bool bWalled;

    Pass(bool bWalled_):bWalled(bWalled_){}
};

struct Maze
{
    std::vector<Spot*> vSpots;
};

void RecursiveMaze(Maze& mz);
void WilsonMaze(Maze& mz);

struct PentaMaze
{
    std::vector<Pass*> v;
    std::map<FiveCoord, Spot*> mp;

    FiveCoord fcExit;

    Spot* GetSpot(FiveCoord fc);

    PentaMaze(FiveCoord fcBnd);
    ~PentaMaze();

    void GetMaze(Maze& mz);

    void InitLevel(std::map<FiveCoord, WallDef>& mpLevel);
};